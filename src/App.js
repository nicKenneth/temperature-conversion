import React from 'react';
import './App.css';
import Input from './components/input.js'

class App extends React.Component {

  state = { //single source of truth
    temperature: '',
    celsius: '',
    fahrenheit: '',
    kelvin:'',
    error:false
  }

  temConversion = (e) =>{

    if (e.target.value === ''){
      console.log('null')
      this.setState(
        {
          celsius:'',
          fahrenheit:'',
          kelvin:'',
          error:false
        }
      )
      return  
    }

    if(e.target.value === '-'){
      this.setState(
        {
          celsius:'-',
          fahrenheit:'-',
          kelvin:'-',
          error:false
        }
      )

      return 
    }

    let val = parseInt(e.target.value)
    console.log(val)

    if(Number.isNaN(val)) {
      console.log('Not a Number')
      this.setState({error:true})
      return 
    }

    if (e.target.name ==='celsius'){
      this.setState(
        {
          temperature:'celsius',
          celsius: val,
          fahrenheit: (9/5*val+32).toFixed(3),
          kelvin:(val+273).toFixed(3),
          error:false
        }
      )
    }
    

    else if (e.target.name ==='fahrenheit'){
      this.setState(
        {
          temperature:'fahrenheit',
          celsius: ((val-32)*5/9).toFixed(3),
          fahrenheit: val,
          kelvin: ((val-32)*5/9 + 273).toFixed(3),
          error:false
        }
      )
    }

    else if (e.target.name ==='kelvin'){
      this.setState(
        {
          temperature:'kelvin',
          celsius: (val-273).toFixed(3),
          fahrenheit: (9/5*(val-273 )+32).toFixed(3),
          kelvin:val,
          error:false
        }
      )
    }
  }

  componentDidMount(){
    
  }

  render(){

    return(
      <div className = "AppUtama">
        <h1>Temperature Converter</h1>
        <h4>Temperature Values: {this.state.temperature}</h4>

        {
          this.state.error ?
          <p className="NotaNumber"> Input a right number ! </p> : null
        }

        <Input 
          name="celsius" 
          temperature={this.state.temperature}
          value={this.state.celsius}
          changevalue = {this.temConversion}
        />


        <Input 
          name="fahrenheit" 
          temperature={this.state.temperature}
          value={this.state.fahrenheit}
          changevalue = {this.temConversion}
        />

        <Input 
          name="kelvin" 
          temperature={this.state.temperature}
          value={this.state.kelvin}
          changevalue = {this.temConversion}
        />

        
      </div>
    );
  }
}

export default App;