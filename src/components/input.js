import React from 'react'

class Input extends React.Component{
    render(){
        return( // cuma bole 1 child <div></div>
            <div>
                <div className = "inputTemp">

                <label
                    {...this.props.temperature === this.props.name && {style:{fontWeight:'bold'}} }>
                    {this.props.name} 
                </label> 

                <input className="form-control"
                    id={this.props.name}
                    name={this.props.name}
                    value={this.props.value}
                    onChange= {this.props.changevalue}
                    placeholder={`enter a ${this.props.name} value`}/>
                </div>
            </div>
        );
     }
}

export default Input;